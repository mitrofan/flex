import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import './App.css';

function App() {
  return (
    <React.Fragment>
      <div className="flexContainer">
        <div className="fixedSizeElement">Hello, world!</div>
        <Select className="longSelect" placeholder="Select something" options={[
          { label: 'Option 1', value: 1 },
          { label: 'Long Option 2', value: 2 },
          { label: 'Very Long Option 3', value: 3 }
        ]} value={1}/>
        <Select className="shortSelect" options={[
          { label: '1', value: 1 },
          { label: '2', value: 2 },
          { label: '3', value: 3 }
        ]} clearable={false} value={1} />
        <input className="input" type="text" value="" placeholder="Enter some text" onChange={() => { }} />
        <input className="smallButton" type="button" value="-" />
      </div>

      <div className="flexContainer">
        <div className="fixedSizeElement">Hello!</div>
        <Select className="longSelect" placeholder="Select something" options={[
          { label: 'Option 1' },
          { label: 'Long Option 2' },
          { label: 'Very Long Option 3' }
        ]} />
        <Select className="shortSelect" options={[
          { label: '1', value: 1 },
          { label: '2', value: 2 },
          { label: '3', value: 3 }
        ]} clearable={false} value={1} />
        <input className="input" type="text" value="" placeholder="Enter some text" onChange={() => { }} />
        <input className="smallButton" type="button" value="-" />
      </div>
    </React.Fragment>
  );
}

export default App;
